# Descomplicando o GitLab3

Estou repetindo o treinamento da aula 1 Descompricando Gitlab, criado no dia 17/05/2021



### Day-1
`` bash
 - Entendemos o que é o Git
 - Entendemos o que é Working Dir, Index e HEAD
 - Entendemos o que é o Gitlab
 - Como criar um grupo no Gitlab
 - Como criar um repositório Git
 - Aprendemos os comandos básicos para manitupalão de arquivos e diretórios no git
 - Como criar uma branch
 - Como criar um Merge Request
 - Como adicionar um membro no projeto
 - Como fazer um Merge na Master/Main
```
